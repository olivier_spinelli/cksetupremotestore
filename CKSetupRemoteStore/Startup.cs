using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using CK.AspNet;
using CK.Core;
using System.IO;
using Microsoft.Extensions.Configuration;
using System.Reflection;
using CSemVer;
using Microsoft.Extensions.FileProviders;
using CKSetup;

namespace CKSetupRemoteStore
{
    public class Startup
    {
        public Startup( IConfiguration conf )
        {
            Configuration = conf;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices( IServiceCollection services )
        {
            services.AddOptions();
            services.Configure<CKSetupStoreOptions>( Configuration.GetSection( "store" ) );
            services.AddMemoryCache();
            services.AddSingleton<ComponentDBProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure( IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory )
        {
            var monitor = new ActivityMonitor( "Pipeline configuration." );
            if( env.IsDevelopment() )
            {
                loggerFactory.AddConsole();
                app.UseDeveloperExceptionPage();
            }
            app.UseRequestMonitor();
            app.UseMiddleware<CKSetupStoreMiddleware>();
            app.Run( async ( context ) =>
            {
                var dbInfo = context.RequestServices.GetRequiredService<ComponentDBProvider>().Info;
                var a = (AssemblyInformationalVersionAttribute) Attribute.GetCustomAttribute( Assembly.GetExecutingAssembly(), typeof( AssemblyInformationalVersionAttribute ) );
                var v = new InformationalVersion( a?.InformationalVersion );
                var componentsToDisplay = new[] { "CKSetupInstaller", "CKSetup" };

                await context.Response.WriteAsync(
                    $@"<html>
                        <head>
                            <style>
                                table {{ border-collapse: collapse; }}
                                td, th {{ border-left: solid 1px black; padding: 0px 21px; }}
                                td:first-child, th:first-child{{ border-left: none; }}
                            </style>
                        </head>
                        <body>
                            <h1>Welcome to { env.ApplicationName }.</h1>
                            Version: { v.ToString() }.<br>

                            <hr>{ WriteTable( componentsToDisplay ) }

                            <hr><h2>Statistics</h2>
                            - { dbInfo.NamedComponentCount } named components.<br>
                            - { dbInfo.TotalComponentCount } versioned components covering { dbInfo.TotalComponentCountPerFramework.Count } frameworks:<br>
                            { WriteComponentsPerFramework( dbInfo.TotalComponentCountPerFramework ) }
                            - Stored Files: { dbInfo.StoredFilesCount } - { dbInfo.StoredTotalFilesSize / 1024 } KiB <br>
                            - Without file sharing: { dbInfo.ComponentsFilesCount } - { dbInfo.ComponentsTotalFilesSize / 1024 } KiB <br>
                            { WriteFiles( dbInfo.BiggestFiles, "Biggest" ) }
                        </body>
                    </html>" );
            } );
            monitor.MonitorEnd();
        }

        private static string WriteTable( IReadOnlyCollection<string> components )
        {
            if( components.Count == 0 )
                return string.Empty;

            var s = components.Aggregate( "<table><tr>", ( current, component ) => current + $"<th><h3>{ component }</h3></th>" );
            s = components.Aggregate( $"{s}</tr><tr>", ( current, component ) => current + $"<td>{ WriteColumn( component ) }</td>" );
            return $"{s}</tr></table>";
        }

        private static string WriteColumn( string name )
        {
            return
                $@"<h4>Latest (CI)</h4>
                - <a href=""/dl-zip/{ name }/Net461/ci"">{ name }.zip</a> (Net461)<br>
                - <a href=""/dl-zip/{ name }/NetCoreApp20/ci"">{ name }.zip</a> (NetCoreApp20)<br>
                - <a href=""/dl-zip/{ name }/NetCoreApp21/ci"">{ name }.zip</a> (NetCoreApp21)

                <h4>Current Preview</h4>
                - <a href=""/dl-zip/{ name }/Net461/preview"">{ name }.zip</a> (Net461)<br>
                - <a href=""/dl-zip/{ name }/NetCoreApp20/preview"">{ name }.zip</a> (NetCoreApp20)<br>
                - <a href=""/dl-zip/{ name }/NetCoreApp21/preview"">{ name }.zip</a> (NetCoreApp21)

                <h4>Current Release</h4>
                - <a href=""/dl-zip/{ name }/Net461/release"">{ name }.zip</a> (Net461)<br>
                - <a href=""/dl-zip/{ name }/NetCoreApp20/release"">{ name }.zip</a> (NetCoreApp20)<br>
                - <a href=""/dl-zip/{ name }/NetCoreApp21/release"">{ name }.zip</a> (NetCoreApp21)";
        }

        private static string WriteComponentsPerFramework( IReadOnlyDictionary<TargetFramework, int> dictionary )
        {
            if( dictionary.Count == 0 )
                return string.Empty;

            return dictionary.Aggregate( string.Empty, ( current, fc ) => current + $"&nbsp;&nbsp;- {fc.Key} ({fc.Value} versioned components).<br>" );
        }

        private static string WriteFiles( IReadOnlyCollection<ComponentFile> files, string title )
        {
            if( files.Count == 0 )
                return string.Empty;

            return files.Take( 4 ).Aggregate( $"- {title} files:<br>", ( current, f ) => current + $"&nbsp;&nbsp;- {f.ToDisplayString()}<br>" );
        }
    }
}
