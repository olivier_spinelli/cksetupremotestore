using Cake.Common.Build;
using Cake.Common.Diagnostics;
using Cake.Common.IO;
using Cake.Common.Solution;
using Cake.Common.Tools.MSBuild;
using Cake.Core;
using Cake.Core.Diagnostics;
using SimpleGitVersion;
using System;
using System.Linq;
using Cake.Common.Tools.DotNetCore;
using Cake.Common.Tools.DotNetCore.Publish;

namespace CodeCake
{
    [AddPath( "%UserProfile%/.nuget/packages/**/tools*" )]
    public partial class Build : CodeCakeHost
    {

        public Build()
        {
            Cake.Log.Verbosity = Verbosity.Diagnostic;

            const string solutionName = "CKSetupRemoteStore";
            const string solutionFileName = solutionName + ".sln";

            var releasesDir = Cake.Directory( "CodeCakeBuilder/Releases" );
            var projects = Cake.ParseSolution( solutionFileName )
                                       .Projects
                                       .Where( p => !(p is SolutionFolder)
                                                    && p.Name != "CodeCakeBuilder" );

            // We do not publish .Tests projects for this solution.
            var projectsToPublish = projects
                                        .Where( p => !p.Path.Segments.Contains( "Tests" ) );

            // The SimpleRepositoryInfo should be computed once and only once.
            SimpleRepositoryInfo gitInfo = Cake.GetSimpleRepositoryInfo();
            // This default global info will be replaced by Check-Repository task.
            // It is allocated here to ease debugging and/or manual work on complex build script.
            CheckRepositoryInfo globalInfo = new CheckRepositoryInfo { Version = gitInfo.SafeNuGetVersion };

            Task( "Check-Repository" )
                .Does( () =>
                {
                    globalInfo = StandardCheckRepository( projectsToPublish, gitInfo );
                } );

            Task( "Clean" )
                .Does( () =>
                 {
                     Cake.CleanDirectories( projects.Select( p => p.Path.GetDirectory().Combine( "bin" ) ) );
                     Cake.CleanDirectories( releasesDir );
                     Cake.DeleteFiles( "Tests/**/TestResult*.xml" );
                 } );

            Task( "Build" )
                .IsDependentOn( "Check-Repository" )
                .IsDependentOn( "Clean" )
                .Does( () =>
                 {
                     StandardSolutionBuild( solutionFileName, gitInfo, globalInfo.BuildConfiguration );
                     // Required to have a published version of the runner that unit tests can use with
                     // the right version.
                     var publishSettings = new DotNetCorePublishSettings().AddVersionArguments( gitInfo, c =>
                     {
                         c.Configuration = globalInfo.BuildConfiguration;
                     } );
                     publishSettings.Framework = "netcoreapp2.0";
                     Cake.DotNetCorePublish( "CKSetupInstaller/CKSetupInstaller.csproj", publishSettings );
                     publishSettings.Framework = "netcoreapp2.1";
                     Cake.DotNetCorePublish( "CKSetupInstaller/CKSetupInstaller.csproj", publishSettings );
                 } );

            Task( "Unit-Testing" )
                .IsDependentOn( "Build" )
                .WithCriteria( () => Cake.InteractiveMode() == InteractiveMode.NoInteraction
                                     || Cake.ReadInteractiveOption( "RunUnitTests", "Run Unit Tests?", 'Y', 'N' ) == 'Y' )
               .Does( () =>
                 {
                     if( System.IO.Directory.Exists( "CKSetupRemoteStore/Store" ) )
                     {
                         Cake.Information( "Deleting existing CKSetupRemoteStore/Store." );
                         System.IO.Directory.Delete( "CKSetupRemoteStore/Store", true );
                     }
                     StandardUnitTests( globalInfo.BuildConfiguration, projects.Where( p => p.Name.EndsWith( ".Tests" ) ) );
                 } );

            Task( "Push-Installer-To-Remote-Store" )
                .IsDependentOn( "Unit-Testing" )
                .WithCriteria( () => gitInfo.IsValid )
                .Does( () =>
                {
                    var components = new string[]
                    {
                        $"CKSetupInstaller/bin/{globalInfo.BuildConfiguration}/net461",
                        $"CKSetupInstaller/bin/{globalInfo.BuildConfiguration}/netcoreapp2.0/publish",
                        $"CKSetupInstaller/bin/{globalInfo.BuildConfiguration}/netcoreapp2.1/publish"
                    };

                    var storeConf = Cake.CKSetupCreateDefaultConfiguration();
                    if( globalInfo.IsLocalCIRelease )
                    {
                        storeConf.TargetStoreUrl = System.IO.Path.Combine( globalInfo.LocalFeedPath, "CKSetupStore" );
                    }
                    if( !storeConf.IsValid )
                    {
                        Cake.Information( "CKSetupStoreConfiguration is invalid. Skipped push to remote store." );
                        return;
                    }

                    Cake.Information( $"Using CKSetupStoreConfiguration: {storeConf}" );

                    if( !Cake.CKSetupAddComponentFoldersToStore( storeConf, components ) )
                    {
                        Cake.TerminateWithError( "Error while registering components in local temporary store." );
                    }

                    if( !Cake.CKSetupPushLocalToRemoteStore( storeConf ) )
                    {
                        Cake.TerminateWithError( "Error while pushing components to remote store." );
                    }
                } );

            // This task can be run manually:
            //
            //      -target=Build-And-Push-CKRemoteStore-WebSite
            //
            // This task does the build.
            // The appsettings.json is not included in the project.
            // (the <ExcludeFilesFromDeployment>appsettings.json</ExcludeFilesFromDeployment> in
            // pubxml seems to be ignored).
            Task( "Build-And-Push-CKRemoteStore-WebSite" )
                .IsDependentOn( "Unit-Testing" )
                .WithCriteria( () => gitInfo.IsValidRelease )
                .Does( () =>
                {
                    var publishPwd = Cake.InteractiveEnvironmentVariable( "PUBLISH_CKSetupRemoteStore_PWD" );
                    if( !String.IsNullOrWhiteSpace( publishPwd ) )
                    {
                        var conf = new MSBuildSettings();
                        conf.Configuration = globalInfo.BuildConfiguration;
                        conf.Targets.Add( "Build" );
                        conf.Targets.Add( "Publish" );
                        conf.WithProperty( "DeployOnBuild", "true" )
                            .WithProperty( "PublishProfile", "CustomProfile" )
                            .WithProperty( "UserName", "ci@invenietis.net" )
                            .WithProperty( "Password", publishPwd );
                        conf.AddVersionArguments( gitInfo );

                        Cake.MSBuild( "CKSetupRemoteStore/CKSetupRemoteStore.csproj", conf );
                    }
                } );

            // The Default task for this script can be set here.
            Task( "Default" )
                .IsDependentOn( "Build-And-Push-CKRemoteStore-WebSite" )
                .IsDependentOn( "Push-Installer-To-Remote-Store" );
        }

    }
}
