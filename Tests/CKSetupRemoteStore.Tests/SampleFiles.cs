using CK.Core;
using CK.Text;
using CKSetup;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CK.Testing.MonitorTestHelper;

namespace CKSetupRemoteStore.Tests
{
    public static class SampleFiles
    {
        static File[] _samples;

        public class File
        {
            public readonly SHA1Value SHA1;
            public readonly NormalizedPath Path;
            public readonly int Length;

            public File( NormalizedPath path )
            {
                Path = path;
                SHA1 = SHA1Value.ComputeFileSHA1( path );
                Length = (int)new FileInfo( path ).Length;
            }

            public override string ToString()
            {
                return $"Name=\"{Path.LastPart}\" FileVersion=\"0.0.0.0\" SHA1=\"{SHA1}\" Length=\"{Length}\"";
            }
        }

        public static IReadOnlyList<File> Samples
        {
            get
            {
                if( _samples == null )
                {
                    _samples = Directory.EnumerateFiles( TestHelper.SolutionFolder, "*.*", System.IO.SearchOption.AllDirectories )
                                            .Select( f => new File( f ) )
                                            .Take( 20 )
                                            .ToArray();
                }
                return _samples;
            }
        }

        public static string XmlFilesFor( params int[] fileNumber )
        {
            StringBuilder b = new StringBuilder();
            b.AppendLine( "<Files>" );
            foreach( var idx in fileNumber ) b.Append( "<File " ).Append( Samples[idx] ).AppendLine( "/>" );
            b.AppendLine( "</Files>" );
            return b.ToString();
        }
    }
}
