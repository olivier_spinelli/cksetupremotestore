using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace CKSetupInstaller
{
    internal static class InputHelper
    {
        private static readonly string[] YesNo = { "yes", "no" };
        private static readonly string YesNoHelper = YesNo.GetHelper();

        private static string GetHelper( this IEnumerable<string> @this )
        {
            return $"( {string.Join( " | ", @this )} ) ";
        }

        /// <summary>
        /// A yes or no question.
        /// </summary>
        /// <returns>The answer.</returns>
        internal static bool YesNoQuestion()
        {
            string answer;
            do
            {
                Console.Write( YesNoHelper );
                answer = Console.ReadLine()?.ToLower();
            } while( !YesNo.Contains( answer ) );

            Debug.Assert( !string.IsNullOrEmpty( answer ) );

            return answer.Equals( "yes" );
        }

        /// <summary>
        /// A basic question with custom choices.
        /// Helper will be created based on <paramref name="answers"/>.
        /// </summary>
        /// <param name="answers"></param>
        /// <returns>The answer.</returns>
        internal static string ChoiceQuestion( IEnumerable<string> answers )
        {
            if( answers == null ) throw new NullReferenceException( nameof( answers ) );

            string answer;
            var enumerable = answers as string[] ?? answers.ToArray();
            var helper = enumerable.GetHelper();
            do
            {
                Console.Write( helper );
                answer = Console.ReadLine()?.ToLower();
            } while( !enumerable.Contains( answer ) );

            Debug.Assert( !string.IsNullOrEmpty( answer ) );

            return answer;
        }
    }
}
