namespace CKSetupInstaller
{
    public enum ExitStatus
    {
        Success,
        Abort,
        Failure
    }
}
