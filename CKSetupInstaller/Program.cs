using System;

namespace CKSetupInstaller
{
    internal class Program
    {
        private static void Main( string[] args )
        {

            var installer = new Installer();
            var exitCode = installer.Execute();

            Console.WriteLine("");
            switch( exitCode )
            {
                case ExitStatus.Success:
                    Console.WriteLine( "Installation successful!" );
                    break;

                case ExitStatus.Abort:
                    Console.WriteLine( "Installation aborted." );
                    break;

                case ExitStatus.Failure:
                    Console.WriteLine( "Installation failed." );
                    break;

                default:
                    Console.WriteLine( "An error occurred while installing." );
                    break;
            }

            Console.WriteLine( "Press any key to exit..." );
            Console.ReadKey();
        }
    }
}
