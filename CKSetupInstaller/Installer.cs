using System;
using System.IO;
using System.Net.Http;
using System.IO.Compression;
using System.Threading.Tasks;
using System.Linq;

namespace CKSetupInstaller
{
    public class Installer
    {
#if NET461
        private const string Framework = "Net461";
#else
        private const string Framework = "NetCoreApp20";
#endif

        private const string ExecutableName = "CKSetup";

        private static readonly string[] AllowedBuilds = { "release", "preview", "ci" };
        private static readonly string XMLUrl = @"http://cksetup.invenietis.net/component-info/CKSetup";
        private static readonly string DownloadUrl = @"http://cksetup.invenietis.net/dl-zip/CKSetup";
        private static readonly string ProgramFiles = Environment.GetFolderPath( Environment.SpecialFolder.ProgramFiles );
        private static readonly string InstallationPath = Path.Combine( ProgramFiles, ExecutableName );
        private static readonly string TempPath = Path.Combine( Path.GetTempPath(), ExecutableName );
        private static readonly string TempFilePath = Path.Combine( TempPath, $"{ExecutableName}.zip" );
        private static readonly string TempExtractedPath = Path.Combine( TempPath, ExecutableName );

        public ExitStatus Execute()
        {
            try
            {
                Console.WriteLine( "This utility will install or update CK.Setup to last version on your machine" );

                Console.WriteLine( $"\nDetected framework: {Framework}" );
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine( $"Installation framework will be {Framework}.\n" );
                Console.ForegroundColor = ConsoleColor.White;

                Console.WriteLine(
@"Press ^C at any time to quit.
Please note doing so could arm your current installation.

Which build would you like to install ?" );
                var requestedBuild = InputHelper.ChoiceQuestion( AllowedBuilds );

                Console.WriteLine( "\nRetrieving version information..." );
                var remoteVersion = VersionHelper.GetRemoteVersion( $"{XMLUrl}/{Framework}/{requestedBuild}" );
                var localVersion = VersionHelper.GetLocalVersion( InstallationPath, ExecutableName );

                if( localVersion != null )
                {
                    Console.WriteLine( "" );

                    if( remoteVersion > localVersion )
                    {
                        Console.WriteLine( "NEWEST version is REMOTE." );
                        Console.WriteLine( "\nPlease confirm update." );
                        if( !InputHelper.YesNoQuestion() ) return ExitStatus.Abort;
                    }
                    else if( remoteVersion < localVersion )
                    {
                        Console.WriteLine( "NEWEST version is LOCAL." );
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine( "Warning, installing server version will result into a downgrade. This can lead to bugs or retro compatibility issues." );
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine( "\nPlease confirm downgrade." );
                        if( !InputHelper.YesNoQuestion() ) return ExitStatus.Abort;
                    }
                    else
                    {
                        Console.WriteLine( "Local version is the SAME as remote one." );
                        Console.WriteLine( "\nWould you like to perform a clean installation ?" );
                        if( !InputHelper.YesNoQuestion() ) return ExitStatus.Abort;
                    }
                }
                else
                {
                    Console.WriteLine( "\nPlease confirm installation." );
                    if( !InputHelper.YesNoQuestion() ) return ExitStatus.Abort;
                }

                Console.WriteLine( "\nPreparing installation..." );
                CleanDirectory( TempPath );

                var downloadUrl = $"{DownloadUrl}/{Framework}/{requestedBuild}";
                Console.WriteLine( $"Downloading remote version ({downloadUrl} => {TempFilePath})." );
                DownloadZip( downloadUrl ).GetAwaiter().GetResult();
                Console.WriteLine( "Download complete!" );

                Console.WriteLine( "Extracting files..." );
                ExtractZip();
                Console.WriteLine( "Extraction complete!" );

                Console.WriteLine( "Copying files..." );
                CleanDirectory( InstallationPath );
                CopyFiles();
#if !NET461
                CreateBatFile();
#endif
                Console.WriteLine( "Copy complete!" );

                Console.WriteLine( "Setting path..." );
                SetEnvironmentVariable();
                Console.WriteLine( "Done" );

                return ExitStatus.Success;
            }
            catch( Exception exception )
            {
                Console.WriteLine( $"Something went wrong: {exception.Message}" );
                Console.WriteLine( exception.StackTrace );
                return ExitStatus.Failure;
            }
            finally
            {
                CleanDirectory( TempPath );
            }
        }

        private static void CleanDirectory( string path )
        {
            var directoryInfo = new DirectoryInfo( path );
            if( !directoryInfo.Exists ) return;

            foreach( var directory in directoryInfo.EnumerateDirectories() )
                directory.Delete( true );
            foreach( var file in directoryInfo.EnumerateFiles() )
                file.Delete();
        }

        private static async Task DownloadZip( string url )
        {
            if( !Directory.Exists( TempPath ) ) Directory.CreateDirectory( TempPath );
            var downloadUri = new Uri( url );
            using( var httpClient = new HttpClient() )
            using( var requestStream = await httpClient.GetStreamAsync( downloadUri ) )
            using( var fileStream = new FileStream( TempFilePath, FileMode.CreateNew, FileAccess.Write, FileShare.None, 4096, true ) )
            {
                await requestStream.CopyToAsync( fileStream );
            }
        }

        public static void ExtractZip()
        {
            if( !Directory.Exists( TempExtractedPath ) ) Directory.CreateDirectory( TempExtractedPath );
            ZipFile.ExtractToDirectory( TempFilePath, TempExtractedPath );
        }

        private static void CopyFiles()
        {
            if( !Directory.Exists( InstallationPath ) ) Directory.CreateDirectory( InstallationPath );
            foreach( var file in Directory.GetFiles( TempExtractedPath ) )
            {
                var fileName = Path.GetFileName( file );
                var destinationFile = Path.Combine( InstallationPath, fileName );
                File.Copy( file, destinationFile, true );
            }
        }

        private static void CreateBatFile()
        {
            const string content = "@dotnet \"%~dp0\\CKSetup.dll\" %*";
            using( var streamWriter = new StreamWriter( Path.Combine( InstallationPath, $"{ExecutableName}.bat" ) ) )
            {
                streamWriter.WriteLine( content );
            }
        }

        private static void SetEnvironmentVariable()
        {
            const string name = "Path";
            var value = Environment.GetEnvironmentVariable( name );

            if( value != null && value.Split( ';' ).Contains( InstallationPath, StringComparer.InvariantCultureIgnoreCase ) ) return;

            value += $";{InstallationPath}";
            Environment.SetEnvironmentVariable( name, value, EnvironmentVariableTarget.Machine );
        }
    }
}
