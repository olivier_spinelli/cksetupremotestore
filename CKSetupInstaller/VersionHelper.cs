using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using CSemVer;

namespace CKSetupInstaller
{
    internal static class VersionHelper
    {
        internal static SVersion GetRemoteVersion( string url )
        {
            var remoteInformation = XDocument.Load( url );

            var remoteVersion = remoteInformation.Root?.Attribute( "Version" )?.Value ?? throw new NullReferenceException( "No remote version found." );
            Console.WriteLine( $"remote version: {remoteVersion}" );

            return SVersion.Parse( remoteVersion );
        }

        internal static SVersion GetLocalVersion( string installationPath, string executableName )
        {
            if( !Directory.Exists( installationPath ) )
            {
                Console.WriteLine( "local version: no installation folder found" );
                return null;
            }

            var filePath = Path.Combine( installationPath, executableName );
            var fileVersion = GetFileNuGetVersion( $"{filePath}.exe" ) ?? GetFileNuGetVersion( $"{filePath}.dll" );

            if( fileVersion == null )
            {
                Console.WriteLine( "local version: no executable found" );
                return null;
            }
            
            return fileVersion;
        }

        private static SVersion GetFileNuGetVersion( string filePath )
        {
            if( !File.Exists( filePath ) ) return null;

            var fileVersionInfo = FileVersionInfo.GetVersionInfo( filePath );
            var productVersion = fileVersionInfo.ProductVersion;

            Console.WriteLine( $"local version: {productVersion}" );

            return InformationalVersion.Parse( productVersion ).NuGetVersion;
        }
    }
}
