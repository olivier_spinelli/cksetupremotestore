using System;

// This tags our installer as a SetupDependency in order to add it to a store.
[assembly: CK.Setup.IsSetupDependency()]

namespace CK.Setup
{
    class IsSetupDependencyAttribute : Attribute
    {
    }
}
